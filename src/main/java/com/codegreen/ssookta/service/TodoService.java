package com.codegreen.ssookta.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codegreen.ssookta.entity.TodoEntity;
import com.codegreen.ssookta.repository.TodoRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class TodoService {

	@Autowired
	TodoRepository todoRepository;

	public Iterable<TodoEntity> findAll() {
		log.info("calling Todos.findAll");
		return todoRepository.findAll();
	}

}
