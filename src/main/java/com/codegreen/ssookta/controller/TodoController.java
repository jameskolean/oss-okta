package com.codegreen.ssookta.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.codegreen.ssookta.service.TodoService;

@Controller
public class TodoController {

	@Autowired
	TodoService todoService;

	@RequestMapping("/todo")
    public String all(Model model) {
        model.addAttribute("todos", todoService.findAll());
        return "todos";
    }

}