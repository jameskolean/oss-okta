package com.codegreen.ssookta.repository;

import org.springframework.data.repository.CrudRepository;

import com.codegreen.ssookta.entity.TodoEntity;

public interface TodoRepository extends CrudRepository<TodoEntity, String> {


}
