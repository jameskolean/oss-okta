package com.codegreen.ssookta.entity;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(of = {}, callSuper = true)
@ToString(callSuper = true)
@Entity(name = "todo")
public class TodoEntity extends PersistableImpl {

	private static final long serialVersionUID = -8331945258267466004L;
	private String title;
	private String detail;
	private boolean completed = false;

}
