package com.codegreen.ssookta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableOAuth2Sso
@SpringBootApplication
@EntityScan(basePackages = { "com.codegreen.ssookta.entity" })
@EnableJpaRepositories({ "com.codegreen.ssookta.repository" })
public class SsoOktaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsoOktaApplication.class, args);
    }
}